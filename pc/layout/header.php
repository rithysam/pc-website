<?php 
    include ('config.php');
    $menus = $db->query('SELECT * FROM menus where active = 1 order by order_menu ASC')->fetchAll();

   
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PC</title>
    <link rel="icon" href="<?php base_url('assets/image/bootstrap-solid.svg') ?>">
    <link rel="stylesheet" href="<?php base_url('assets/css/bootstrap.min.css') ?>">
    <script src="<?php base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?php base_url('assets/js/popper.min.js') ?>"></script>
    <script src="<?php base_url('assets/js/bootstrap.min.js') ?>"></script>

    <style>
      .menu_active{    
          /* padding:5px; */
          /* text-transform: uppercase; */
          display:inline;
          list-style-type:none;
          list-style-position: inside;
          border-bottom: 1px solid red;
      }

      .text_line_height span, .text_line_height *{
        /* line-height: 1.2 !important; */
        font-size: 10px !important;
      }
      
    </style>
   
</head>
<body style="overflow-x: hidden">
<!-- Just an image -->
<div class="container">
<nav class="navbar sticky-top navbar-expand-lg navbar-light bg-primary">
  <a class="navbar-brand" href="<?php base_url('')?>">
    <img src="<?php base_url('assets/image/vdoo.png') ?>" class="rounded bg-white" width="50" height="50" alt="">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse " id="navbarNav">
    <ul class="navbar-nav mr-auto">
      <?php foreach($menus as $menu){ ?>
      <li class="nav-item 
        <?php 
          if(isset($_GET['id']) && $_GET['id'] != ''){
            if($menu['id'] == base64_decode($_GET['id'])){
              echo 'menu_active';
            }
          }else{
            if($menu['id']==1){
              echo 'menu_active';
            }
          }
        ?>
      ">
        <a class="nav-link text-white" href="<?php echo '?id='.base64_encode($menu['id'])  ?>"><?php echo $menu['name'] ?> <span class="sr-only">(current)</span></a>
      </li>
      <?php } ?>
    </ul>
    <a href="#" class="float-right text-white">Tel: 085356767</a>

</nav>

<div class="row">
    <div class="col-sm-12">
