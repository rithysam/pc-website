<?php
    // project directory
    $dir = '/web7/pc/';
    $host = 'http://localhost';

    // base url 
    function base_url($url){
        global $host, $dir;
        echo $host.$dir.$url;
    }

    // define contant variable for root directory with project directory
    define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT'] . $dir);
    include (DOCUMENT_ROOT . 'db/configs_front.php');

    function redirect($url){
        header("location: ".$url);
    }
?>