<?php
class Database {

    protected $connection;
	protected $query;
    protected $show_errors = TRUE;
    protected $query_closed = TRUE;
	public $query_count = 0;

	public function __construct($dbhost = 'localhost', $dbuser = 'root', $dbpass = '', $dbname = '', $charset = 'utf8') {
		$this->connection = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
		if ($this->connection->connect_error) {
			$this->error('Failed to connect to MySQL - ' . $this->connection->connect_error);
		}
		$this->connection->set_charset($charset);
	}

    // select pagination 
    function paginate($sql, $pageno, $no_of_records_per_page = 10){
        $offset = ($pageno-1) * $no_of_records_per_page;

        $total_rows = $this->query("$sql")->numRows();
        $total_pages = ceil($total_rows / $no_of_records_per_page);

        $res_data = $this->query("$sql"." LIMIT $offset, $no_of_records_per_page")->fetchAll();
        $data = array(
            'total_pages' => $total_pages,
            'rows' => $res_data
        );
        return $data;
    }

    function paginate_number($pageno, $total_pages){
        if($pageno <= 1){ $previous_page_disable = 'disabled'; }else{$previous_page_disable = '';}
        if($pageno <= 1){ $previous_page = '#'; } else { $previous_page = "?pageno=".($pageno - 1); }
        if($pageno >= $total_pages){ $next_page_disable = 'disabled'; }else{$next_page_disable = '';}
        if($pageno >= $total_pages){ $next_page = '#'; } else { $next_page = "?pageno=".($pageno + 1); }
        $last_page = '?pageno='. $total_pages;
        echo '
        <ul class="pagination">
            <li><a href="?pageno=1" class="btn btn-primary">First</a></li>
            <li class="">
                <a href="'.$previous_page.'" class="'.$previous_page_disable.' btn btn-primary">Prev</a>
            </li>
            <li class="">
            <a href="'.$next_page.'" class="btn btn-primary '.$next_page_disable.'">Next</a>
            </li>
            <li><a href="'.$last_page.'" class="btn btn-primary">Last</a></li>
        </ul>';
    }


    // SQL query will be passed through this function

    // mysqli_query();
    public function query($query) {
        if (!$this->query_closed) {
            $this->query->close();
        }
		if ($this->query = $this->connection->prepare($query)) {
            if (func_num_args() > 1) {
                $x = func_get_args();
                $args = array_slice($x, 1);
				$types = '';
                $args_ref = array();
                foreach ($args as $k => &$arg) {
					if (is_array($args[$k])) {
						foreach ($args[$k] as $j => &$a) {
							$types .= $this->_gettype($args[$k][$j]);
							$args_ref[] = &$a;
						}
					} else {
	                	$types .= $this->_gettype($args[$k]);
	                    $args_ref[] = &$arg;
					}
                }
				array_unshift($args_ref, $types);
                call_user_func_array(array($this->query, 'bind_param'), $args_ref);
            }
            $this->query->execute();
           	if ($this->query->errno) {
				$this->error('Unable to process MySQL query (check your params) - ' . $this->query->error);
           	}
            $this->query_closed = FALSE;
			$this->query_count++;
        } else {
            $this->error('Unable to prepare MySQL statement (check your syntax) - ' . $this->connection->error);
        }
		return $this;
    }

    // Fetch multiple row from a database
    // eg. echo $account['name'];
    // mysqli_fetch_assco()
	public function fetchAll($callback = null) {
	    $params = array();
        $row = array();
	    $meta = $this->query->result_metadata();
	    while ($field = $meta->fetch_field()) {
	        $params[] = &$row[$field->name];
	    }
	    call_user_func_array(array($this->query, 'bind_result'), $params);
        $result = array();
        while ($this->query->fetch()) {
            $r = array();
            foreach ($row as $key => $val) {
                $r[$key] = $val;
            }
            if ($callback != null && is_callable($callback)) {
                $value = call_user_func($callback, $r);
                if ($value == 'break') break;
            } else {
                $result[] = $r;
            }
        }
        $this->query->close();
        $this->query_closed = TRUE;
		return $result;
	}

    // Fetch a records from a database
	public function fetchArray() {
	    $params = array();
        $row = array();
	    $meta = $this->query->result_metadata();
	    while ($field = $meta->fetch_field()) {
	        $params[] = &$row[$field->name];
	    }
	    call_user_func_array(array($this->query, 'bind_result'), $params);
        $result = array();
		while ($this->query->fetch()) {
			foreach ($row as $key => $val) {
				$result[$key] = $val;
			}
		}
        $this->query->close();
        $this->query_closed = TRUE;
		return $result;
	}

    // Close the database:
	public function close() {
		return $this->connection->close();
	}

    // Get the number of rows:
    public function numRows() {
		$this->query->store_result();
		return $this->query->num_rows;
	}

    // Get the affected number of rows:
	public function affectedRows() {
		return $this->query->affected_rows;
	}

    // Get the last insert ID:
    public function lastInsertID() {
    	return $this->connection->insert_id;
    }

    public function error($error) {
        if ($this->show_errors) {
            exit($error);
        }
    }

    public function real_escape_string($string){
        return $this->connection->real_escape_string($string);
    }

	private function _gettype($var) {
	    if (is_string($var)) return 's';
	    if (is_float($var)) return 'd';
	    if (is_int($var)) return 'i';
	    return 'b';
	}

}
?>