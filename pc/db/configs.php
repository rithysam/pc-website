<?php 
    include ('db.php');

    $dbhost = 'localhost';
    $dbuser = 'root';
    $dbpass = '';
    $dbname = 'pc_db';

    $db = new Database($dbhost, $dbuser, $dbpass, $dbname);
    session_start();
    if($_SESSION['is_login'] != 1){
        header('location: http://localhost/web7/pc/admin/index.php');
    }
?>