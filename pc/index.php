<?php
    include ('layout/header.php');
  
    $brands = $db->query("SELECT * FROM brands where active = 1")->fetchAll();
    

    $banners = $db->query("SELECT * FROM banners where active = 1 ")->fetchAll();
?>
    <!-- Banner  -->
    <section class="py-3">
        <div id="demo" class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ul class="carousel-indicators">
                <?php foreach($banners as $key => $b){ ?>
                    <li data-target="#demo" data-slide-to="<?php echo $key ?>" class="<?php echo $key?'active':'' ?>"></li>
                <?php } ?>
            </ul>

            <!-- The slideshow -->
            <div class="carousel-inner">
                <?php foreach($banners as $key => $banner){ ?>
                <div class="carousel-item <?php echo $key==0? 'active': '' ?>">
                    <img src="<?php base_url('assets/uploads/banners/'.$banner['photo']) ?>" al="<?php echo $banner['id'] ?>" width="100%" height="300">
                </div>
                <?php } ?>
            </div>

            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>
    </section>

    <!-- product  -->
    <?php foreach($brands as $brand){  $brand_id = $brand['id']?>
       <div class="row">
        <div class="col ">
            <h4 id="<?php echo $brand_id ?>" class="p-2 border border-primary rounded text-primary">
                <img class="rounded-circle" src="<?php base_url('assets/uploads/brands/'.$brand['photo']) ?>" alt="Image" width="40" height="40">
                <?php echo $brand['name'] ?>
            </h4>
        </div>
       </div>
       <div class="row">
            <?php 
                if(isset($_GET['id']) && $_GET['id'] != ''){
                    $id = base64_decode($_GET['id']);
                    $products = $db->query("SELECT * FROM products where active = 1 and brand_id = $brand_id and menu_id = $id")->fetchAll();
                }else{
                    $products = $db->query("SELECT * FROM products where active = 1 and brand_id = $brand_id  and menu_id = 1")->fetchAll();
                }
        
                foreach($products as $p){
            ?>
            <div class="col-sm-3 p-3">
                <div class="card" style="width: 100%;">
                    <?php
                        $path = 'http://localhost/web7/pc/assets/uploads/products/'.$p['photo'];
                        if(@getimagesize($path)){
                    ?>
                            <img src="<?php base_url('assets/uploads/products/'.$p['photo']) ?>" class="card-img-top" alt="Image" height="180">

                    <?php
                        }else{
                    ?>
                        <img src="<?php base_url('assets/image/image-not-found.jpeg') ?>" class="card-img-top" alt="Image" height="180">
                    <?php
                        }
                    ?>
                
                    <div class="card-body" style="">
                        <p class=" text-center text-primary"><?php echo $p['name'] ?></p>
                        
                        <h5 class="card-title text-center ">
                            <?php 
                                if($p['discount']){
                                    echo "<del class='text-default'>$" . $p['price'].  "</del>  ";
                                    echo "<span class='text-danger'>$" . ($p['price']-($p['price']*$p['discount']/100)) . " </span>";
                                }
                            ?>
                        
                        </h5>
                        <div class="text_line_height" stlyle="overflow: hidden">
                            <?php echo $p['description'] ?>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php }  ?>
        </div>
        <?php } ?>
  

<?php
    include ('layout/footer.php');
?>
 