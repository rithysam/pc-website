<?php
    include ('../layout/header.php');
    $id = $_GET['id'];
    $users = $db->query("SELECT * FROM brands WHERE id = $id");
    $user = $users->fetchArray();
?>

<div class="card border-0">
    <div class="card-body">
        <div class="row mb-2">
            <div class="col-sm-12">
                <a href="index.php" class="btn btn-primary btn-sm"> Go Back</a>
            </div>
        </div>
        <h4>Edit Brand</h3>
      
        <div class="row">
            <div class="col-sm-12">
                <form action="action_edit.php" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo $id ?>">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Brand Name <span class="text-danger">*</span></label>
                            <input type="text" name="name" value="<?php echo $user['name'] ?>" class="form-control" require>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Logo</label>
                            <input type="file" name="photo" class="form-control">
                        </div>
                    </div> 
                   
                    <button class="btn btn-primary">Save</button>

                </form>
            </div>
        </div>
    </div>
</div>
 