<?php
    include ('../layout/header.php');
?>

<div class="card border-0">
    <div class="card-body">
        <h4>List Users</h3>
        <div class="row mb-3">
            <div class="col-sm-12">
                <a href="create.php" class="btn btn-sm btn-primary">Create</a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-bordered">
                    <thead>
                       <tr>
                            <th>#</th>
                            <th>Photo</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Actions</th>
                       </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $users = $db->query('SELECT * FROM users where active = 1 ORDER BY id DESC')->fetchAll();
                        ?>
                        <?php foreach($users as $key => $p){ ?>
                            <tr>
                                <td><?php echo $key + 1 ?></td>
                                <td>
                                    <img src="<?php base_url('assets/uploads/users/'.$p['photo']) ?>" alt="" width="50px" height="50px">
                                </td>
                                <td><?php echo $p['name'] ?></td>
                                <td><?php echo $p['username'] ?></td>
                                <td>
                                    <a href="edit.php?id=<?php echo $p['id'] ?>" class="btn btn-sm btn-success">Edit</a>
                                    <a href="action_delete.php?id=<?php echo $p['id'] ?>" onclick="return confirm('Are you sure you want to delete?')" class="btn btn-sm btn-danger">Delete</a>

                                </td>
                            </tr>
                        <?php } ?>
                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
 