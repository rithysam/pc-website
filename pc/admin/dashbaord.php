<?php
    include ('layout/header.php');
    $product_count = $db->query("SELECT count(*) as total FROM products where active = 1")->fetchArray();
    $brand_count = $db->query("SELECT count(*) as total FROM brands where active = 1")->fetchArray();
    $user_count = $db->query("SELECT count(*) as total FROM users where active = 1")->fetchArray();
?>

<div class="card border-0">
    <div class="card-body">
        <h3>Dashboard</h3>

        <div class="row">
            <div class="col-sm-4">
               <div class="bg-primary p-3">
                    <br>
                    <h3>Products</h3>
                    <h4><?php echo $product_count['total'] ?></h4>
                    <br>
               </div>
            </div>
            <div class="col-sm-4">
                <div class="bg-warning p-3">
                    <br>
                    <h3>Brands</h3>
                    <h4><?php echo $brand_count['total'] ?></h4>
                    <br>
                </div>
            </div>

            <div class="col-sm-4">
               <div class="bg-success p-3">
                    <br>
                    <h3>Users</h3>
                    <h4><?php echo $user_count['total'] ?></h4>
                    <br>
               </div>
            </div>
        </div>

    </div>
</div>
 