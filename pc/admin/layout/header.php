<?php 
    include ('config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PC</title>
    <link rel="icon" href="<?php base_url('assets/image/bootstrap-solid.svg') ?>">
    <link rel="stylesheet" href="<?php base_url('assets/css/bootstrap.min.css') ?>">
    <script src="<?php base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?php base_url('assets/js/popper.min.js') ?>"></script>
    <script src="<?php base_url('assets/js/bootstrap.min.js') ?>"></script>
</head>
<body style="overflow-x: hidden">
<!-- Just an image -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">
    <img src="<?php base_url('assets/image/bootstrap-solid.svg') ?>" width="30" height="30" alt="">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?php base_url('admin/dashbaord.php') ?>">Dashobard <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php base_url('admin/products') ?>">Products</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php base_url('admin/brands') ?>">Brands</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php base_url('admin/banners') ?>">Banner</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php base_url('admin/menus') ?>">Menu</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?php base_url('admin/users') ?>">User</a>
      </li>
  
      <li class="nav-item">
     
      </li>
    </ul>
    <a href="<?php base_url('admin/logout.php') ?>" class="float-right">Logout</a>
</nav>

<div class="row">
    <div class="col-sm-12">
