<?php
    include ('../layout/header.php');
?>

<div class="card border-0">
    <div class="card-body">
        <div class="row mb-2">
            <div class="col-sm-12">
                <a href="index.php" class="btn btn-primary btn-sm"> Go Back</a>
            </div>
        </div>
        <h4>Create Banner</h3>
      
        <div class="row">
            <div class="col-sm-12">
                <form action="action_create.php" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Banner Name <span class="text-danger">*</span></label>
                            <input type="text" name="name" class="form-control" require>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Banner</label>
                            <input type="file" name="photo" class="form-control">
                        </div>
                      
                    </div> 

                    <button class="btn btn-primary">Save</button>

                </form>
            </div>
        </div>
    </div>
</div>
 