<?php
    include ('../layout/header.php');
    $brands = $db->query('SELECT * FROM brands WHERE active = 1')->fetchAll();
    $menus = $db->query('SELECT * FROM menus WHERE active = 1')->fetchAll();
?>

<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<div class="card border-0">
    <div class="card-body">
        <div class="row mb-2">
            <div class="col-sm-12">
                <a href="index.php" class="btn btn-primary btn-sm"> Go Back</a>
            </div>
        </div>
        <h4>Create Product</h3>
      
        <div class="row">
            <div class="col-sm-12">
                <form action="action_create.php" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Prodcut Name <span class="text-danger">*</span></label>
                            <input type="text" name="name" class="form-control" require>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Price <span class="text-danger">*</span></label>
                            <input type="text" name="price" class="form-control" require>
                        </div>
                    </div> 

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Brand <span class="text-danger">*</span></label>
                            <select name="brand"  class="form-control" required>
                                <option value="">-- Choose a brand --</option>
                                <?php 
                                    foreach($brands as $b){
                                ?>
                                    <option value="<?php echo $b['id'] ?>"><?php echo $b['name'] ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Menu  <span class="text-danger">*</span></label>
                            <select name="menus"  class="form-control" required>
                                <option value="">-- Choose a menu --</option>
                                <?php 
                                    foreach($menus as $m){
                                ?>
                                    <option value="<?php echo $m['id'] ?>"><?php echo $m['name'] ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div> 

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Specail Product <span class="text-danger">*</span></label>
                            <select name="is_specail" class="form-control" >
                                <option value="">-- Choose an option --</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Photo</label>
                            <input type="file" name="photo" class="form-control">
                        </div>
                    </div> 
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Discount <span class="text-danger"></span></label>
                            <input type="number" step="0.1" name="discount" class="form-control">
                        </div>
                    </div> 

                    <div class="row">
                        <div class="form-group col-12">
                            <textarea id="summernote" name="description"></textarea>
                        </div>
                    </div>


                    <button class="btn btn-primary">Save</button>

                </form>
            </div>
        </div>
    </div>
</div>
 
 <script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
 </script>