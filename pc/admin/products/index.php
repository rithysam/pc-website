<?php
    include ('../layout/header.php');
    // $actual_link = "http://$_SERVER[HTTP_HOST]";
    // $url = $actual_link;
    // $query = parse_url($url, PHP_URL_QUERY);
    // echo $query;
   
    // // Returns a string if the URL has parameters or NULL if not
    // if ($query) {
    //     $url .= '&category=1';
    // } else {
    //     $url .= '?category=1';
    // }
?>

<?php 
                            

                            if (isset($_GET['pageno'])) {
                                $pageno = $_GET['pageno'];
                            } else {
                                $pageno = 1;
                            }
                            if(isset($_GET["search"]) && $_GET["search"] != ""){
                                $search = $_GET['search'];
                                $sql = "SELECT products.*, brands.name as brand_name FROM products INNER JOIN brands ON brands.id = products.brand_id where products.active = 1 and `name` LIKE '%".$search."%'";
                            }else{
                                $sql = "SELECT products.*, brands.name as brand_name FROM products INNER JOIN brands ON brands.id = products.brand_id where products.active = 1 order by products.id desc";
                            }
                            $no_of_records_per_page = 20;
                            $result = $db->paginate($sql, $pageno, $no_of_records_per_page);
                            $i = 1;
                            if($pageno> 1){
                                $i = $pageno*$no_of_records_per_page-($no_of_records_per_page-1);
                            }
                            // $products = $db->query('SELECT * FROM products')->fetchAll();
                        ?>

<div class="card border-0">
    <div class="card-body">
        <h4>List Products</h3>
        <div class="row mb-3">
            <div class="col-sm-12">
                <a href="create.php" class="btn btn-sm btn-primary">Create</a>
                <form action="" class="float-right">
                   <div class="row">
                        <div class="col-sm-8 p-0">
                            <input type="text" name="search" class="form-control" placeholder="Type product name">
                        </div>
                        <div class="col pl-0">
                            <button class="btn btn-primary btn-block">Search</button>
                        </div>
                   </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
            <div class=""><?php $db->paginate_number($pageno, $result['total_pages']);?></div>

                <table class="table table-bordered">
                    <thead>
                       <tr>
                            <th>#</th>
                            <th>Photo</th>
                            <th>Brand</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Discount</th>
                            <th>Special</th>
                            <th>Status</th>
                            <th>Actions</th>
                       </tr>
                    </thead>
                    <tbody>
                       
                        <?php foreach($result['rows'] as $key => $p){ ?>
                            <tr>
                                <td><?php echo $i++ ?></td>
                                <td>
                                    <img src="<?php base_url('assets/uploads/products/'.$p['photo']) ?>" alt="" width="50px" height="50px">
                                </td>
                                <td><?php echo $p['brand_name'] ?></td>
                                <td><?php echo $p['name'] ?></td>
                                <td><?php echo $p['price'] ?>$</td>
                                <td><?php echo $p['discount'] ?>%</td>
                                <td><?php echo $p['is_special']?'Yes':'No' ?></td>
                                <td><?php echo $p['active']?'Active':'Inactive' ?></td>
                                <td>
                                    <a href="edit.php?id=<?php echo $p['id'] ?>">Edit</a>
                                    <a href="action_delete.php?id=<?php echo $p['id'] ?>"  onclick="return confirm('Are you sure you want to delete?')" class="text-danger"><?php echo $p['active']?'Delete':'Activate' ?></a>
                                </td>
                            </tr>
                        <?php } ?>
                    
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
 