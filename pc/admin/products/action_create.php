<?php
    include ('../layout/header.php');
    $name = $_POST['name'];
    $price = $_POST['price'];
    $brand = $_POST['brand'];
    $menu = $_POST['menus'];
    $is_specail = $_POST['is_specail'];
    $discount = $_POST['discount'];
    $description = $db->real_escape_string($_POST['description']);
    $file_name = '';
    if(!empty($_FILES['photo']['name'])){
        $target_dir = '../../assets/uploads/products/';
        $orginal_name = $_FILES['photo']['name'];
        $file_type = pathinfo($orginal_name, PATHINFO_EXTENSION);
        $file_name = date('dmy_his').'.'.$file_type; 
        $path_to_upload = $target_dir.$file_name;
        move_uploaded_file($_FILES['photo']['tmp_name'], $path_to_upload);
    }

    $query = "INSERT INTO products (
            `name`, 
            `price`, 
            `brand_id`,
            `menu_id`,
            `is_special`,
            `discount`,
            `description`,
            `photo`
        ) 
        VALUE(
            '$name', 
            '$price', 
            '$brand', 
            '$menu',
            '$is_specail',
            '$discount',
            '$description',
            '$file_name'
        )";
    
    $insert = $db->query($query);
    if($insert->affectedRows()){
        redirect('index.php');
    }else{
        redirect('create.php');
    }
    

?>