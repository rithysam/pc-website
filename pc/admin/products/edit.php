<?php
    include ('../layout/header.php');
    $id = $_GET['id'];
    $brands = $db->query('SELECT * FROM brands WHERE active = 1')->fetchAll();
    $menus = $db->query('SELECT * FROM menus WHERE active = 1')->fetchAll();
    $product_query = "SELECT products.* FROM products 
        INNER JOIN brands ON brands.id = products.brand_id
        INNER JOIN menus ON menus.id = products.menu_id
        WHERE products.id = $id
        ";
    $product = $db->query($product_query)->fetchArray();
?>

<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<div class="card border-0">
    <div class="card-body">
        <div class="row mb-2">
            <div class="col-sm-12">
                <a href="index.php" class="btn btn-primary btn-sm"> Go Back</a>
            </div>
        </div>
        <h4>Create Product</h3>
      
        <div class="row">
            <div class="col-sm-12">
                <form action="action_edit.php" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo $id ?>">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Prodcut Name <span class="text-danger">*</span></label>
                            <input type="text" name="name" value="<?php echo $product['name'] ?>" class="form-control" require>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Price <span class="text-danger">*</span></label>
                            <input type="text" name="price" value="<?php echo $product['price'] ?>"  class="form-control" require>
                        </div>
                    </div> 

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Brand <span class="text-danger">*</span></label>
                            <select name="brand"  class="form-control" required>
                                <option value="">-- Choose a brand --</option>
                                <?php 
                                    foreach($brands as $b){
                                ?>
                                    <option value="<?php echo $b['id'] ?>" <?php if($b['id'] == $product['brand_id']) echo 'selected'; else echo '';?>><?php echo $b['name'] ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Menu  <span class="text-danger">*</span></label>
                            <select name="menus"  class="form-control" required>
                                <option value="">-- Choose a menu --</option>
                                <?php 
                                    foreach($menus as $m){
                                ?>
                                    <option value="<?php echo $m['id'] ?>" <?php if($m['id'] == $product['menu_id']) echo 'selected'; else echo '';?> ><?php echo $m['name'] ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div> 

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Specail Product <span class="text-danger">*</span></label>
                            <select name="is_specail" class="form-control" >
                                <option value="">-- Choose an option --</option>
                                <option value="1" <?php if(1 == $product['is_special']) echo 'selected'; else echo '';?>>Yes</option>
                                <option value="0" <?php if(0 == $product['is_special']) echo 'selected'; else echo '';?>>No</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Photo</label>
                            <input type="file" name="photo" class="form-control">
                        </div>
                    </div> 
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Discount <span class="text-danger"></span></label>
                            <input type="number" step="0.1" name="discount" value="<?php echo $product['discount'] ?>" class="form-control">
                        </div>
                        <div class="form-group col-sm-6">
                        <img src="<?php base_url('assets/uploads/products/'.$p['photo']) ?>" alt="" width="50px" height="50px">
                        </div>
                    </div> 

                    <div class="row">
                        <div class="form-group col-12">
                            <textarea id="summernote" name="description"><?php echo $product['description'] ?></textarea>
                        </div>
                    </div>


                    <button class="btn btn-primary">Save</button>

                </form>
            </div>
        </div>
    </div>
</div>
 
 <script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
 </script>