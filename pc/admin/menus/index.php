<?php
    include ('../layout/header.php');
?>

<div class="card border-0">
    <div class="card-body">
        <h4>List Menus</h3>
        <div class="row mb-2">
            <div class="col-sm-12">
                <a href="create.php" class="btn btn-primary btn-sm">Create</a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-bordered">
                    <thead>
                       <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Order</th>
                            <th>Status</th>
                            <th>Actions</th>
                       </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $menus = $db->query('SELECT * FROM menus')->fetchAll();
                        ?>
                        <?php foreach($menus as $key => $menu){ ?>
                            <tr>
                                <td><?php echo $key + 1 ?></td>
                                <td><?php echo $menu['name'] ?></td>
                                <td><?php echo $menu['order_menu'] ?></td>
                                <td><?php echo $menu['active']?'Active': "Inactive" ?></td>
                                <td>
                                    <?php if($menu['id'] != 1){ ?>
                                    <a href="edit.php?id=<?php echo $menu['id'] ?>" class="btn btn-sm btn-success">Edit</a>
                                    <a href="action_delete.php?id=<?php echo $menu['id']?>" onclick="return confirm('Are you sure you want to delete?')" class='btn btn-sm btn-danger'>Delete</a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
 