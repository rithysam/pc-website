<?php
    include ('../layout/header.php');
    $id = $_GET['id'];
    $menu = $db->query("SELECT * FROM menus WHERE id = $id")
            ->fetchArray();

    $last_order = $db->query('SELECT * FROM menus where active = 1 order by id DESC LIMIT 1' )->fetchArray();
    $menus = $db->query("SELECT * FROM menus where active = 1 and id != $id")->fetchAll();

    
?>

<div class="card border-0">
    <div class="card-body">
        <div class="row mb-2">
            <div class="col-sm-12">
                <a href="index.php" class="btn btn-primary btn-sm"> Go Back</a>
            </div>
        </div>
        <h4>Edit Menu</h3>
      
        <div class="row">
            <div class="col-sm-12">
                <form action="action_edit.php" method="POST">
                    <input type="hidden" name="id" value="<?php echo $id ?>" required>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Menu Name <span class="text-danger">*</span></label>
                            <input type="text" name="name" value="<?php echo $menu['name'] ?>" class="form-control" require>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Order <span class="text-danger">*</span></label>
                            <!-- <select name="order" id="" class="form-control">
                                <option value="<?php echo @$last_order['id']+1 ?> ">Last</option>
                                <?php foreach($menus as $m){ ?>
                                <option value="<?php echo $m['id']-1 ?>" <?php echo ($menu['order_menu'] == $m['id']+1)? "selected" : "" ?> >Before <?php echo $m['name'] ?></option>
                                <?php } ?>
                            </select> -->
                            <input type="number" name="order" class="form-control" required>
                        </div>

                    </div> 
                    <button class="btn btn-primary">Save</button>

                </form>
            </div>
        </div>
    </div>
</div>
 