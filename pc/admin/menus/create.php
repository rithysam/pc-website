<?php
    include ('../layout/header.php');
    $menus = $db->query('SELECT * FROM menus where active = 1 ')->fetchAll();
    $last_order = $db->query('SELECT * FROM menus where active = 1 order by id DESC LIMIT 1' )->fetchArray();

?>

<div class="card border-0">
    <div class="card-body">
        <div class="row mb-2">
            <div class="col-sm-12">
                <a href="index.php" class="btn btn-primary btn-sm"> Go Back</a>
            </div>
        </div>
        <h4>Create Menu</h3>
      
        <div class="row">
            <div class="col-sm-12">
                <form action="action_create.php" method="POST">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Menu Name <span class="text-danger">*</span></label>
                            <input type="text" name="name" class="form-control" require>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Order <span class="text-danger">*</span></label>
                            <!-- <select name="order" id="" class="form-control">
                                <option value="<?php echo @$last_order['id']+1 ?> ">Last</option>
                                <?php foreach($menus as $m){ ?>
                                <option value="<?php echo $m['id']-1 ?>">Before <?php echo $m['name'] ?></option>
                                <?php } ?>
                            </select> -->
                            <input type="number" name="order" class="form-control" required>

                        </div>

                    </div> 
                    <button class="btn btn-primary">Save</button>

                </form>
            </div>
        </div>
    </div>
</div>
 